# Python-Übung 6 - Regression

Die 6. Python-Übung der Vorlesung Mathematik für Studierende der Chemie II.

## Themen des Übungszettels
* Einlesen von Daten mit numpy.
* Visualisierung mit matplotlib.
* Regression mit numpy.

## Nutzung

1. Anmeldung auf [jupyter-cloud](https://jupyter-cloud.gwdg.de) mit den
   studentischen Zugangsdaten (Stud.IP, FlexNow, ...) oder dem GWDG-Account.
2. In der oberen Menüleiste: Reiter **Git** 👉🏿 **Clone a Repository**.
3. Es öffnet sich ein Popup-Dialog. Hier den HTTPS-Clone-Link dieses Projekts eintragen: 
   [https://gitlab.gwdg.de/math-for-chemists-two/exercise-6.git](https://gitlab.gwdg.de/math-for-chemists-two/exercise-6.git)
4. Im neuen Ordner auf das Jupyter-Notebook navigieren.
5. Fertig!

## Autoren

* Alea Miako Tokita
